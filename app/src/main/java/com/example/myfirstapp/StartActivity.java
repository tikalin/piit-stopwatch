package com.example.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;

public class StartActivity extends AppCompatActivity {
    public static final String EXERCISE_TIME = "com.example.myfirstapp.EXERCISE_TIME";
    public static final String REST_TIME = "com.example.myfirstapp.REST_TIME";
    public static final String NUM_REPEATS = "com.example.myfirstapp.NUM_REPEATS";
    public static final String NUM_EXERCISES = "com.example.myfirstapp.NUM_EXERCISES";
    public static final String REPEAT_REST_TIME = "com.example.myfirstapp.REPEAT_REST_TIME";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, RunActivity.class);

        Spinner exerciseTimeSpinner = (Spinner) findViewById(R.id.exerciseTime);
        String exerciseTime = exerciseTimeSpinner.getSelectedItem().toString();
        intent.putExtra(EXERCISE_TIME, exerciseTime);

        Spinner restTimeSpinner = (Spinner) findViewById(R.id.restTime);
        String restTime = restTimeSpinner.getSelectedItem().toString();
        intent.putExtra(REST_TIME, restTime);

        Spinner numRepeatsSpinner = (Spinner) findViewById(R.id.numRepeats);
        String numRepeats = numRepeatsSpinner.getSelectedItem().toString();
        intent.putExtra(NUM_REPEATS, numRepeats);

        Spinner numExercisesSpinner = (Spinner) findViewById(R.id.numExercises);
        String numExercises = numExercisesSpinner.getSelectedItem().toString();
        intent.putExtra(NUM_EXERCISES, numExercises);

        Spinner repeatRestTimeSpinner = (Spinner) findViewById(R.id.repeatRestTime);
        String repeatRestTime = repeatRestTimeSpinner.getSelectedItem().toString();
        intent.putExtra(REPEAT_REST_TIME, repeatRestTime);

        startActivity(intent);
    }

}
