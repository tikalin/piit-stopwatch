package com.example.myfirstapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.IOException;

public class RunActivity extends AppCompatActivity {
    int exerciseTime;
    int restTime;
    int numExercises;
    int currentExerciseNum;
    int numRepeats;
    int repeatRestTime;
    private Runnable runnable;
    private TextView showTime, showStage;
    private Handler handler = new Handler();
    int totalTime;
    int seconds = 2;
    int next = 1;
    String stage = "Start";
    MediaPlayer mpExercise;
    MediaPlayer mpRest;
    MediaPlayer mpCountdown;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);

        Intent intent = getIntent();
        String exerciseTimeStr = intent.getStringExtra(StartActivity.EXERCISE_TIME);
        String restTimeStr = intent.getStringExtra(StartActivity.REST_TIME);
        String numExercisesStr = intent.getStringExtra(StartActivity.NUM_EXERCISES);
        String numRepeatsStr = intent.getStringExtra(StartActivity.NUM_REPEATS);
        String repeatRestTimeStr = intent.getStringExtra(StartActivity.REPEAT_REST_TIME);

        exerciseTime = Integer.parseInt(exerciseTimeStr);
        restTime = Integer.parseInt(restTimeStr);
        numExercises = Integer.parseInt(numExercisesStr);
        currentExerciseNum = numExercises;
        numRepeats = Integer.parseInt(numRepeatsStr);
        repeatRestTime = Integer.parseInt(repeatRestTimeStr);
        mpCountdown = MediaPlayer.create(this, R.raw.countdown);
        mpExercise = MediaPlayer.create(this, R.raw.exerciseaudio);
        mpRest = MediaPlayer.create(this, R.raw.restaudio);

        //Testing text Views
        /*TextView textView1 = findViewById(R.id.textView);
        textView1.setText(exerciseTime);

        TextView textView2 = findViewById(R.id.textView2);
        textView2.setText(restTime);

        TextView textView3 = findViewById(R.id.textView3);
        textView3.setText(numExercises);

        TextView textView4 = findViewById(R.id.textView4);
        textView4.setText(numRepeats);

        TextView textView5 = findViewById(R.id.textView5);
        textView5.setText(repeatRestTime);*/

        initUi();

        totalTime = 1000*(numRepeats*((numExercises*(exerciseTime+restTime)) + repeatRestTime));

        /*CountDownTimer ct = new CountDownTimer(totalTime, 1000) {

            public void onTick(long millisUntilFinished) {
                showTime.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                showTime.setText("done!");
            }
        };
        ct.start();*/

        runExercise();

    }

    private void initUi() {
        showTime = findViewById(R.id.textView7);
        showStage = findViewById(R.id.textView8);
    }

    private void runExercise() {
        handler.post(new Runnable() {
           @Override
           public void run() {

               if (numRepeats > 0) {
                   showTime.setText(Integer.toString(seconds));
                   showStage.setText(stage);

                   seconds--;
                   if (seconds == -1) {

                       switch (next) {
                           case 1:
                               seconds = exerciseTime;
                               next = 2;
                               stage = "Exercise";
                               if (mpExercise.isPlaying())
                                   mpExercise.stop();
                               mpExercise.start();
                               break;
                           case 2:
                               seconds = restTime;
                               stage = "Rest";
                               currentExerciseNum--;
                               if (currentExerciseNum == 0) {
                                   next = 3;
                               } else {
                                   next = 1;
                               }
                               if (mpRest.isPlaying())
                                   mpRest.stop();
                               mpRest.start();
                               break;
                           case 3:
                               seconds = repeatRestTime;
                               stage = "Repeat rest";
                               numRepeats--;
                               currentExerciseNum = numExercises;
                               next = 1;
                               if (mpRest.isPlaying())
                                   mpRest.stop();
                               mpRest.start();
                               break;
                           default:
                               seconds = 2;
                               break;
                       }
                   }

                   if (seconds == 2) {
                       if (mpCountdown.isPlaying())
                           mpCountdown.stop();
                       mpCountdown.start();
                   }
                   handler.postDelayed(this, 1000);
               }
               else {
                   showTime.setText("done!");
                   showStage.setText("done!");
               }
           }
        });
    }

    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }



}
